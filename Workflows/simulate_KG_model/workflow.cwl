#!/usr/bin/env cwltool

cwlVersion: v1.0
class: Workflow

inputs:
  model_uuid: string
  script_path: string
  script_arguments: string[]
  dataset_version_uuid: string
  token: string

outputs:
  final_output:
    type: double
    outputSource: verify_simulation/difference_score

steps:
  download_KG_model:
    label: Download model from EBRAINS Knowledge Graph
    run: ../../CommandLineTools/download_KG_model/download_KG_model.cwl
    in:
      model_uuid: model_uuid
    out: [downloaded_code]

  run_simulation:
    run: ../../CommandLineTools/run_python_simulation/run_simulation.cwl
    in:
      code_directory: download_KG_model/downloaded_code
      script_path: script_path
      script_arguments: script_arguments
    out: [simulation_results]

  download_reference_data:
    run: ../../CommandLineTools/download_KG_dataset/download_KG_dataset.cwl
    in:
      dataset_version_uuid: dataset_version_uuid
      token: token
    out: [downloaded_data]

  verify_simulation:
    run: ../../CommandLineTools/compare_directories/compare_directories.cwl
    in:
      dir1: run_simulation/simulation_results
      dir2: download_reference_data/downloaded_data
    out: [difference_score]

s:identifier: https://kg.ebrains.eu/api/instances/3baadd82-bd19-4554-97b0-10e0f3000241

$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
