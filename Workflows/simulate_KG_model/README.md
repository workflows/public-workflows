# CWL workflow for running simulations of models in the EBRAINS Knowledge Graph


## Executing the workflow with Toil

```
$ toil-cwl-runner workflow.cwl default_inputs.yml
```