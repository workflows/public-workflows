#!/usr/bin/env python
"""
A very simple script for comparing two directories.

This is a placeholder for Florent's tools.
"""

import os
import json
import click


@click.command()
@click.argument('dir1')
@click.argument('dir2')
def main(dir1, dir2):
    score = 0
    files_dir1 = [
        os.path.join(dirpath, filename)
        for dirpath, dirnames, filenames in os.walk(dir1)
        for filename in filenames
    ]
    files_dir2 = [
        os.path.join(dirpath, filename)
        for dirpath, dirnames, filenames in os.walk(dir2)
        for filename in filenames
    ]
    sizes1 = [os.stat(file_path).st_size for file_path in files_dir1]
    sizes2 = [os.stat(file_path).st_size for file_path in files_dir2]
    for size in sizes1:
        if size not in sizes2:
            score += 1
    score /= len(sizes1)
    print(json.dumps({
        "difference_score": score,
        "sizes1": sizes1,
        "sizes2": sizes2,
        "files_dir1": files_dir1,
        "files_dir2": files_dir2
    }, indent=2))


if __name__ == '__main__':
    main()