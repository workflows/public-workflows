cwlVersion: v1.0
class: CommandLineTool
baseCommand: compare_directories.py
label: "Check whether two directories contain the same files"
hints:
  DockerRequirement:
    dockerImageId: compare_directories
inputs:
  dir1:
    type: Directory
    inputBinding:
      position: 1
  dir2:
    type: Directory
    inputBinding:
      position: 2
stdout: cwl.output.json
outputs:
  difference_score: double

s:identifier: https://kg.ebrains.eu/api/instances/bf783996-2657-4a74-ba8d-185e0656dab6
s:keywords: ["data analysis"]

$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
