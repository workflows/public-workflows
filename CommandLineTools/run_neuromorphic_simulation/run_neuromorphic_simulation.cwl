cwlVersion: v1.2
class: CommandLineTool
baseCommand: [nmpi, run]
label: "Runs a neuromorphic computing job"
intent: [https://openminds.ebrains.eu/instances/softwareFeature/simulation]
hints:
  DockerRequirement:
    dockerImageId: docker-registry.ebrains.eu/workflow-components/run-nmc-simulation:arm64
requirements:
  - class: InlineJavascriptRequirement
  - class: NetworkAccess
    networkAccess: true
inputs:
  script:
    type: File
    inputBinding:
        position: 1
  token:
    type: string
    inputBinding:
      position: 0
      prefix: --token=
      separate: false
  collab_id:
    type: string
    inputBinding:
      position: 0
      prefix: --collab-id=
      separate: false
  platform:
    type: string
    inputBinding:
      position: 0
      prefix: --platform=
      separate: false
  output_dir:
    type: string?
    inputBinding:
      position: 0
      prefix: --output-dir=
      separate: false
  tags:
    type:
      type: array
      items: string
      inputBinding:
        prefix: --tag=
        separate: false
    inputBinding:
      position: 0
outputs:
  results:
    type: Directory
    outputBinding:
      glob: $(inputs.output_dir)/*

s:identifier: https://kg.ebrains.eu/api/instances/2c4f3cd8-335d-49b7-b001-c8600ed49cac
s:keywords: ["simulation"]

$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
