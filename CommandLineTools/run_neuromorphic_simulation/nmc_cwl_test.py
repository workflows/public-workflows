import sys
exec("import pyNN.%s as sim" % sys.argv[1])

sim.setup()

p = sim.Population(2, sim.IF_cond_exp(i_offset=0.1))
p.record("v")

sim.run(100.0)

p.write_data("simulation_data.pkl")

sim.end()
