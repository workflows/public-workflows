cwlVersion: v1.0
class: CommandLineTool
baseCommand: download_KG_dataset.py
label: "Downloads the data for a dataset identified by an EBRAINS KnowledgeGraph UUID."
hints:
  DockerRequirement:
    dockerImageId: download_kg_dataset
inputs:
  token:
    type: string
    inputBinding:
      position: 1
      prefix: --token
  dataset_version_uuid:
    type: string
    inputBinding:
      position: 3
outputs:
  downloaded_data:
    type: Directory
    outputBinding:
      glob: "*"


s:identifier: https://kg.ebrains.eu/api/instances/a2c18b3b-8455-497e-aa8a-126a5d40bc40
s:keywords: ["data transfer"]

$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
