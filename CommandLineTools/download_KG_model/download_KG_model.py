#!/usr/bin/env python
"""
This script downloads the code for a model identified by an EBRAINS KnowledgeGraph UUID
to a local directory.

Author: Andrew Davison, CNRS
Year: 2023
"""

import os
import re
import tarfile

import requests
import click


patterns = {
    "github release": r"https:\/\/github.com\/(?P<org>\w+)\/(?P<repo>[-\w]+)\/releases\/tag\/(?P<version>[-.\w]+)"
}

download_url_templates = {
     "github release": "https://github.com/{org}/{repo}/archive/refs/tags/{version}.tar.gz"
}


@click.command()
@click.argument('uuid')
@click.option('-d', '--download-dir', default="downloads",
              help='The directory into which code will be downloaded.')
def main(uuid, download_dir):
    # uuid = "c407130b-1e06-486c-a02c-aeb0ccbc7eca"

    url = f"https://validation.brainsimulation.eu/models/query/instances/{uuid}"

    response = requests.get(url)
    assert response.status_code == 200

    model_version = response.json()
    code_location = model_version["source"]

    download_url = None
    for label, pattern in patterns.items():
        match = re.match(pattern, code_location)
        if match:
            download_url = download_url_templates[label].format(**match.groupdict())

    print(f"DOWNLOADING {download_url}")

    os.makedirs("downloads", exist_ok=True)
    local_filename = f"downloads/{match['repo']}-{match['version']}.tar.gz"

    with open(local_filename, "wb") as fp:
        response = requests.get(download_url)
        assert response.status_code == 200, f"{response.content} - {download_url}"
        fp.write(response.content)

    print("SUCCESSFUL DOWNLOAD")

    with tarfile.open(local_filename) as tf:
        tf.extractall("downloads")

    os.remove(local_filename)


if __name__ == '__main__':
    main()
    #print("Hello world")