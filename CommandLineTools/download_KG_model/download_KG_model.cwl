cwlVersion: v1.0
class: CommandLineTool
baseCommand: download_KG_model.py
label: "Download the code for a model identified by an EBRAINS KnowledgeGraph UUID"
hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/reproducibility/cwl-workflows/download_kg_model@sha256:d5590da8876330e95faea5c21de003689da18c8e80e1e96d7beb6a1a05a4afbf
inputs:
  model_uuid:
    type: string
    inputBinding:
      position: 1
outputs:
  downloaded_code:
    type: Directory
    outputBinding:
      glob: "downloads/*"

s:identifier: https://kg.ebrains.eu/api/instances/1768d516-7a10-4407-85cf-fef43dc5a680
s:keywords: ["data transfer"]
s:author:
  - class: s:Person
    s:identifier: http://orcid.org/0000-0002-4793-7541
    s:email: mailto:andrew.davison@cnrs.fr
    s:name: Andrew P. Davison
s:codeRepository: https://gitlab.ebrains.eu:workflows/public-workflows.git
s:dateCreated: "2023-02-13"
s:license: https://spdx.org/licenses/MIT
s:programmingLanguage: Python

omv:fullName: Download a public model from the EBRAINS Knowledge Graph
omv:shortName: download_kg_model
omv:developer:
  - class: omc:Person
    omv:givenName: Andrew
    omv:familyName: Davison
    omv:digitalIdentifier:
      - class: omc:ORCID
        omv:identifier: http://orcid.org/0000-0002-4793-7541
omv:description: "Download the code for a model identified by an EBRAINS Knowledge Graph UUID"
omv:homepage: https://gitlab.ebrains.eu:workflows/public-workflows.git
omv:accessibility: https://openminds.ebrains.eu/instances/productAccessibility/freeAccess
omv:copyright:
  - class: omc:Copyright
    omv:year: 2023
    omv:holders:
    - class: omc:Organization
      s.identifier: https://kg.ebrains.eu/api/instances/31259b06-91d0-4ad8-acfd-303fc9ed613b
      omv:fullName: French National Centre for Scientific Research
      omv:shortName: CNRS
      omv:digitalIdentifier:
        - class: omc:RORID
          omv:identifier: https://ror.org/02feahw73
omv:format:
  - class: omc:ContentType
    s.id: https://openminds.ebrains.eu/instances/contentTypes/application/vnd.commonworkflowlanguage.commandlinetool
omv:license:
  - class: omc.License
    s.id: https://openminds.ebrains.eu/instances/licenses/mit
omv:repository:
  - class: omc:FileRepository
    omv:iri: https://gitlab.ebrains.eu:workflows/public-workflows.git  # todo: point to tag
omv:supportChannel:
  - andrew.davison@cnrs.fr
  - https://ebrains.eu/support
omv:versionIdentifier: v0.1.0
omv:versionInnovation: first version


$namespaces:
 s: https://schema.org/
 omv: https://openminds.ebrains.eu/vocab/
 omc: https://openminds.ebrains.eu/core/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
