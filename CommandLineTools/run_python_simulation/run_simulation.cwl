cwlVersion: v1.2
class: CommandLineTool
baseCommand: python
label: "Runs a simulation script written in Python"
intent: [https://openminds.ebrains.eu/instances/softwareFeature/simulation]
hints:
  DockerRequirement:
    dockerImageId: neuralensemble/simulation:arm64_pynn0101
requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.code_directory)
inputs:
  code_directory:
    type: Directory
  script_path:
    type: string
    inputBinding:
      position: 1
  script_arguments:
    type: string[]
    inputBinding:
      position: 2
outputs:
  simulation_results:
    type: Directory
    outputBinding:
      glob: Results/*

s:identifier: https://kg.ebrains.eu/api/instances/2c4f3cd8-335d-49b7-b001-c8600ed49cac
s:keywords: ["simulation"]

$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
